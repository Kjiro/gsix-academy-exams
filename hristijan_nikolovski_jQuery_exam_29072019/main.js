const psv = (users) =>{
		var html='';

		for(var i=0; i<users.length; i++){
			var fullname = users[i].name;
			var username = users[i].username;
			var email = users[i].email;
			var city = users[i].address.city;

			html += `<tr>
						<td>${fullname}</td>
						<td>${username}</td>
						<td>${email}</td>
						<td>${city}</td>
						<td>
							<button mclass="table-success" class="btn btn-sm btn-success">
								Success
							</button>

							<button mclass="table-warning" class="btn btn-sm btn-warning">
								Warning
							</button>

							<button mclass="table-danger" class="btn btn-sm btn-danger">
								Disable
							</button>

							<button rem="rem" class="btn btn-sm btn-dark">
								Remove
							</button>
						</td>
					</tr>`;
		}
		$("#tbody").html(html);
	}

$(document).ready(function(){
	
	$.ajax({
		method: "GET",
		url: "https://jsonplaceholder.typicode.com/users",
		success: function(users){
			psv(users); //za 102,3% xD
		}
	}) 
	//AJAX END HERE

	$(document).on("click", "button", function(){
		if($(this).attr("rem")){//mozit i vaka da ne prajt sporedba, deka nemame potreba od 
			if($(this).attr("pos"))//druga vrednost za toj atribut na niedno drugo mesto
				$("table tbody").empty();
			else $(this).parent().parent().remove();
		} //go izvadov nadvor za da dobijame na modularnost,
		  //kodot od prvobitnata verzija mi e komentiran podolu

		else if($(this).attr("pos")=="top"){
			// if($(this).attr("rem")=="rem")
			// 	$("table tbody").empty();
			
			$("table tbody tr").removeClass();
			$("table tbody tr").addClass($(this).attr("mclass"));
		}
		else{
			// if($(this).attr("rem")=="rem")
			// 	$(this).parent().parent().remove();

			$(this).parent().parent().removeClass();
			$(this).parent().parent().addClass($(this).attr("mclass"));
		}
	});
});