class Car{
	constructor(make, model, year, km, country){
		this._make=make;
		this._model=model;
		this._year=year;
		this._km=km;
		this._country=country;
	}

	get make(){
		return this._make;
	}
	set make(newMake){
		this._make = newMake;
	}

	get model(){
		return this._model;
	}
	set model(newModel){
		this._model = newModel;
	}

	get year(){
		return this._year;
	}
	set year(newYear){
		this._year = newYear;
	}

	get km(){
		return this._km;
	}
	set km(newKm){
		this._km = newKm;
	}

	get country(){
		return this._country;
	}
	set country(newCountry){
		this._country = newCountry;
	}
}
//po nalog na daskalot pisuvam dokumentacija :D
//ova pogore e klasata za operacija so koli

const prikazi=(event)=>{
	event.preventDefault();

	document.getElementById('text_fullname').innerHTML=document.getElementById('fullname').value;
	document.getElementById('text_email').innerHTML=document.getElementById('email').value;

	document.getElementById('fullname').value = '';
	document.getElementById('email').value = '';

	document.getElementById('fullname').focus();
}
document.getElementById('saveInfo').addEventListener('click', prikazi);
//ova pogore e kompletniot event listener za prviot segment od zadacava


var cars=[];

var lista = [];

const stampaj = niza =>{
	var html='';
	for(var i=0; i<niza.length; i++){
		html += "<tr><td>" + niza[i].make + "</td><td>" + niza[i].model + "</td><td>" + niza[i].year + "</td><td>" + niza[i].km + "</td><td>" + niza[i].country + "</td></tr>"; 
	}
	return html;
} //ova mi e funkcija za bildanje za namaluvanje na redundantnosta

const zacuvaj = event =>{
	event.preventDefault();

	var make = document.getElementById('avtomobil_marka').value;
	var model = document.getElementById('avtomobil_model').value;
	var year = parseInt(document.getElementById('proizvodstvo').value);
	var km = parseInt(document.getElementById('izminati_kilometri').value);
	var country = document.getElementById('zemja_na_poteklo').value;


	var car = new Car(make, model, year, km, country);

	cars.push(car);

	html=stampaj(cars);

	document.getElementById('tbody_sort').innerHTML=html;

	document.getElementById('avtomobil_marka').value = '';
	document.getElementById('avtomobil_model').value = '';
	document.getElementById('proizvodstvo').value = '';
	document.getElementById('izminati_kilometri').value = '';

	document.getElementById('avtomobil_marka').focus();
}
document.getElementById('zacuvajAvtomobil').addEventListener('click', zacuvaj);
//do ovde e event listener za dodavanje na koli v tabela


const sortiraj = () =>{
	
	lista=cars.map(item=>item); //bidejkji ne ni treba orginalniot raspored nemame potreba od duplikatov ovde, ama zaradi moznosta da se dodaj takva funkcija vo idnina odluciv da ima duplikat za nejzino polesno eventualno implementiranje

	if(document.getElementById('sort_select').value == "proizodstvo_najstara"){
		lista.sort(function(a,b){return a.year-b.year});
	}

	else if(document.getElementById('sort_select').value == "proizodstvo_najnova"){
		lista.sort(function(a,b){return b.year-a.year});
	}

	else if(document.getElementById('sort_select').value == "kilometri_najmalku"){
		lista.sort(function(a,b){return a.km-b.km});
	}

	else if(document.getElementById('sort_select').value == "kilometri_najmnogu"){
		lista.sort(function(a,b){return b.km-a.km});
	}

	html=stampaj(lista);

	document.getElementById('tbody_sort').innerHTML=html;
}
document.getElementById('sort').addEventListener('click', sortiraj);
//ova pogore e event listener za funkcijata sortiranje SO DVATA FILTERI :D

const newer = value => {
	return value.year >= 2000;
}
const older = value => {
	return value.year < 2000;
}
//Ovie mi se funkciicki za filtriranje(direkno gi pustam kako parametar vo filtrirackite funkcii)

const filtriraj = () => {
	
	lista=cars.map(item=>item);

	lista=lista.filter(item => item.country == document.getElementById('filter_select_zemja').value)

	if(parseInt(document.getElementById('filter_select_godina').value)){
		lista=lista.filter(newer);
	}
	else lista=lista.filter(older);

	html=stampaj(lista);

	document.getElementById('tbody_filter').innerHTML=html;

}
document.getElementById('filter').addEventListener('click', filtriraj);
//i na kraj event listenerot za filtriranje